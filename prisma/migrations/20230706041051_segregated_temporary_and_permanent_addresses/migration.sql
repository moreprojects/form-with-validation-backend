/*
  Warnings:

  - You are about to drop the column `city` on the `CustomerDetails` table. All the data in the column will be lost.
  - You are about to drop the column `doorNo` on the `CustomerDetails` table. All the data in the column will be lost.
  - You are about to drop the column `state` on the `CustomerDetails` table. All the data in the column will be lost.
  - You are about to drop the column `street` on the `CustomerDetails` table. All the data in the column will be lost.
  - You are about to drop the column `zip` on the `CustomerDetails` table. All the data in the column will be lost.
  - A unique constraint covering the columns `[permanentAddressId]` on the table `CustomerDetails` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[temporaryAddressId]` on the table `CustomerDetails` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `permanentAddressId` to the `CustomerDetails` table without a default value. This is not possible if the table is not empty.
  - Added the required column `temporaryAddressId` to the `CustomerDetails` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "CustomerDetails" DROP COLUMN "city",
DROP COLUMN "doorNo",
DROP COLUMN "state",
DROP COLUMN "street",
DROP COLUMN "zip",
ADD COLUMN     "permanentAddressId" TEXT NOT NULL,
ADD COLUMN     "temporaryAddressId" TEXT NOT NULL,
ALTER COLUMN "dateOfBirth" SET DATA TYPE VARCHAR(12);

-- CreateTable
CREATE TABLE "Address" (
    "id" TEXT NOT NULL,
    "doorNo" VARCHAR(255) NOT NULL,
    "street" VARCHAR(255) NOT NULL,
    "city" VARCHAR(255) NOT NULL,
    "state" VARCHAR(255) NOT NULL,
    "zip" VARCHAR(255) NOT NULL,

    CONSTRAINT "Address_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "CustomerDetails_permanentAddressId_key" ON "CustomerDetails"("permanentAddressId");

-- CreateIndex
CREATE UNIQUE INDEX "CustomerDetails_temporaryAddressId_key" ON "CustomerDetails"("temporaryAddressId");

-- AddForeignKey
ALTER TABLE "CustomerDetails" ADD CONSTRAINT "CustomerDetails_permanentAddressId_fkey" FOREIGN KEY ("permanentAddressId") REFERENCES "Address"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "CustomerDetails" ADD CONSTRAINT "CustomerDetails_temporaryAddressId_fkey" FOREIGN KEY ("temporaryAddressId") REFERENCES "Address"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
