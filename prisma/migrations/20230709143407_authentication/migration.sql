/*
  Warnings:

  - You are about to drop the `Address` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `CustomerDetails` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "CustomerDetails" DROP CONSTRAINT "CustomerDetails_permanentAddressId_fkey";

-- DropForeignKey
ALTER TABLE "CustomerDetails" DROP CONSTRAINT "CustomerDetails_temporaryAddressId_fkey";

-- DropTable
DROP TABLE "Address";

-- DropTable
DROP TABLE "CustomerDetails";

-- CreateTable
CREATE TABLE "user" (
    "id" TEXT NOT NULL,
    "email" TEXT NOT NULL,
    "password" VARCHAR(100) NOT NULL,
    "customer_detailsId" TEXT NOT NULL,

    CONSTRAINT "user_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "customer_details" (
    "id" TEXT NOT NULL,
    "firstName" VARCHAR(30) NOT NULL,
    "lastName" VARCHAR(30) NOT NULL,
    "dateOfBirth" VARCHAR(12) NOT NULL,
    "createdAt" TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "profession" VARCHAR(30) NOT NULL,
    "age" INTEGER NOT NULL,
    "phone" VARCHAR(15) NOT NULL,
    "alternatePhone" VARCHAR(15) NOT NULL,
    "email" VARCHAR(100) NOT NULL,
    "permanentAddressId" TEXT NOT NULL,
    "temporaryAddressId" TEXT NOT NULL,
    "userId" TEXT NOT NULL,

    CONSTRAINT "customer_details_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "address" (
    "id" TEXT NOT NULL,
    "doorNo" VARCHAR(30) NOT NULL,
    "street" VARCHAR(30) NOT NULL,
    "city" VARCHAR(30) NOT NULL,
    "state" VARCHAR(30) NOT NULL,
    "zip" VARCHAR(8) NOT NULL,

    CONSTRAINT "address_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "user_email_key" ON "user"("email");

-- CreateIndex
CREATE UNIQUE INDEX "user_customer_detailsId_key" ON "user"("customer_detailsId");

-- CreateIndex
CREATE UNIQUE INDEX "customer_details_email_key" ON "customer_details"("email");

-- CreateIndex
CREATE UNIQUE INDEX "customer_details_permanentAddressId_key" ON "customer_details"("permanentAddressId");

-- CreateIndex
CREATE UNIQUE INDEX "customer_details_temporaryAddressId_key" ON "customer_details"("temporaryAddressId");

-- CreateIndex
CREATE UNIQUE INDEX "customer_details_userId_key" ON "customer_details"("userId");

-- AddForeignKey
ALTER TABLE "customer_details" ADD CONSTRAINT "customer_details_permanentAddressId_fkey" FOREIGN KEY ("permanentAddressId") REFERENCES "address"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "customer_details" ADD CONSTRAINT "customer_details_temporaryAddressId_fkey" FOREIGN KEY ("temporaryAddressId") REFERENCES "address"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "customer_details" ADD CONSTRAINT "customer_details_userId_fkey" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
