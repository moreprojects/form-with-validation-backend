/*
  Warnings:

  - You are about to drop the column `customer_detailsId` on the `user` table. All the data in the column will be lost.
  - Added the required column `fullName` to the `user` table without a default value. This is not possible if the table is not empty.

*/
-- DropIndex
DROP INDEX "user_customer_detailsId_key";

-- AlterTable
ALTER TABLE "user" DROP COLUMN "customer_detailsId",
ADD COLUMN     "fullName" VARCHAR(30) NOT NULL;
