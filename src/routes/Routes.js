const { Router } = require("express");
const CustomerController = require("../controllers/CustomerController");
const AuthenticationController = require("../controllers/authenticationController");

const customerRouter = Router();
const userRouter = Router();

userRouter.post("/signup", AuthenticationController.signup);
userRouter.post("/login", AuthenticationController.login);

customerRouter.get("/all", CustomerController.getCustomers);
customerRouter.post("/create", CustomerController.createCustomer);
customerRouter.get(
  "/address/:addressId",
  CustomerController.getCustomerAddress
);
customerRouter.put(
  "/address/:addressId",
  CustomerController.updateCustomerAddress
);
customerRouter.put("/:customerId", CustomerController.updateCustomer);
customerRouter.delete("/:customerId", CustomerController.deleteCustomer);

module.exports = { customerRouter, userRouter };
