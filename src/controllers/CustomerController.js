const customerService = require("../services/customerService");

const createCustomer = async (req, res) => {
  try {
    console.log("%c%o", "background-color: green; color: white", req.body.data);
    const { temporaryAddress, permanentAddress, ...details } = req.body.data;
    const { userId } = req;
    const response = await customerService.createCustomer({
      temporaryAddress,
      permanentAddress,
      details,
      userId,
    });
    res.status(200).json(response);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const deleteCustomer = async (req, res) => {
  try {
    let result = await customerService.deleteCustomer(req.params.customerId);
    res.status(200).json(result);
  } catch (error) {
    res.status(500).json({ message: "Internal Server Error" });
  }
};

const getCustomers = async (req, res) => {
  try {
    const result = await customerService.getCustomers(req.userId);
    res.status(200).json(result);
  } catch (error) {
    res.status(500).json({ message: "Internal Server Error" });
  }
};

const updateCustomer = async (req, res) => {
  try {
    const { data } = req.body;
    const { customerId } = req.params;
    const result = await customerService.updateCustomer(customerId, data);
    res.status(200).json(result);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};
const getCustomerAddress = async (req, res) => {
  try {
    const { addressId } = req.params;
    const response = await customerService.getCustomerAddress(addressId);
    res.status(200).json(response);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};
const updateCustomerAddress = async (req, res) => {
  try {
    const { addressId } = req.params;
    const { data } = req.body;
    const response = await customerService.updateCustomerAddress(
      addressId,
      data
    );
    res.status(200).json(response);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

module.exports = {
  getCustomers,
  createCustomer,
  deleteCustomer,
  updateCustomer,
  getCustomerAddress,
  updateCustomerAddress,
};
