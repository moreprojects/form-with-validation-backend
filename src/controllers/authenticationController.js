const authenticationService = require("../services/authenticationService");

const signup = async (req, res) => {
  try {
    const { email, password, fullName } = req.body.data;
    const user = await authenticationService.signup(email, password, fullName);
    res.status(200).json(user);
  } catch (error) {
    console.log(error, "controller");
    res.status(500).json({ message: error.message });
  }
};
const login = async (req, res) => {
  try {
    const { email, password } = req.body.data;
    const token = await authenticationService.login(email, password);
    res.status(200).json({ token: token });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};
module.exports = { signup, login };
