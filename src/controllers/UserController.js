const { getUserDataById } = require("../services/userService");

const getUserData = async (req, res) => {
  try {
    const user = await getUserDataById(req.userId);
    if (!user) {
      res.send(404).json({ message: "Email does not exist" });
    }
    console.log(user);
    res.send(200).json(user);
  } catch (error) {
    console.log(error);
    res.send(500).json({ message: error });
  }
};

module.exports = { getUserData };
