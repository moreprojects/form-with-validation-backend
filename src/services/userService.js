const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();

const getUserDataById = async (userId) => {
  try {
    const userData = await prisma.user.findUnique({
      where: {
        id: userId,
      },
      include: {
        customer_details: {
          permanentAddress: true,
          temporaryAddress: true,
        },
      },
    });
    if (!userData) {
      return userData;
    }
    console.log(userData);
    return userData;
  } catch (error) {
    throw error.message;
  }
};

module.exports = { getUserDataById };
