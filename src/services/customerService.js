const { PrismaClient } = require("@prisma/client");

const prisma = new PrismaClient();

const createCustomer = async ({
  temporaryAddress,
  permanentAddress,
  details,
  userId,
}) => {
  try {
    const response = await prisma.customer_details.create({
      data: {
        ...details,
        temporaryAddress: {
          create: temporaryAddress,
        },
        permanentAddress: {
          create: permanentAddress,
        },
        user: {
          connect: {
            id: userId,
          },
        },
      },
    });
    return response;
  } catch (error) {
    if (error.code === "P2002") {
      throw new Error("Email already Exists");
    } else {
      throw new Error("Not able to create the Customer");
    }
  }
};
const deleteCustomer = async (customerId) => {
  try {
    let result = await prisma.customer_details.delete({
      where: {
        id: customerId,
      },
    });
    return result;
  } catch (error) {
    throw new Error("Internal Server Error");
  }
};

const getCustomers = async (userId) => {
  try {
    const result = await prisma.user.findUnique({
      where: {
        id: userId,
      },
      include: {
        customer_details: {
          include: {
            permanentAddress: true,
            temporaryAddress: true,
          },
        },
      },
    });
    if (result === null) {
      throw new Error("No Data Found");
    } else {
      return result;
    }
  } catch (error) {
    throw new Error("Internal Server Error");
  }
};

const updateCustomer = async (customerId, data) => {
  try {
    const response = await prisma.customer_details.update({
      where: {
        id: customerId,
      },
      data: data,
    });
    return response;
  } catch (error) {
    throw new Error("Not able to Update Data");
  }
};

const getCustomerAddress = async (id) => {
  try {
    const response = await prisma.address.findUnique({
      where: { id },
    });
    return response;
  } catch (error) {
    throw new Error("Cannot Get Customer Address");
  }
};

const updateCustomerAddress = async (id, data) => {
  try {
    const response = await prisma.address.update({
      where: {
        id,
      },
      data,
    });
    return response;
  } catch (error) {
    throw new Error("Cannot Update Customer Address");
  }
};

module.exports = {
  createCustomer,
  deleteCustomer,
  getCustomers,
  updateCustomer,
  getCustomerAddress,
  updateCustomerAddress,
};
