const { PrismaClient } = require("@prisma/client");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const prisma = new PrismaClient();
const dotenv = require("dotenv");
dotenv.config({ path: "../../.env" });

const login = async (email, password) => {
  try {
    const user = await prisma.user.findUnique({
      where: {
        email,
      },
    });

    if (!user) {
      throw new Error("Invalid Credentials");
    }

    const isPasswordValid = await bcrypt.compare(password, user.password);

    if (!isPasswordValid) {
      throw new Error("Incorrect Password");
    }
    const token = jwt.sign({ userId: user.id }, process.env.SECRET_KEY, {
      expiresIn: 60 * 60 * 24,
    });
    // console.log(token);
    return token;
  } catch (error) {
    console.log(error);
    throw new Error(" Failed to login");
  }
};

const signup = async (email, password, fullName) => {
  try {
    const hashedPassword = await bcrypt.hash(password, 10);
    const user = await prisma.user.create({
      data: {
        fullName,
        email,
        password: hashedPassword,
      },
    });
    return user;
  } catch (error) {
    console.log(error, "Service");
    if (error.code === "P2002") throw new Error("Email already exists");
  }
};

module.exports = { login, signup };
