export default async (searchString) => {
  const fieldsToSearch = ["firstName", "lastName", "dateOfBirth"];

  const filter = fieldsToSearch.map((field) => ({
    [field]: {
      contains: searchString,
    },
  }));

  const users = await prisma.user.findMany({
    where: {
      OR: filter,
    },
  });
};
