const express = require("express");
const { customerRouter, userRouter } = require("./src/routes/Routes");
var cors = require("cors");
const authenticationMiddleware = require("./src/middlewares/authenticationMiddleware");

const app = express();
const port = process.env.PORT || 8000;

app.use(cors());
app.use(express.json());

app.use("/user", userRouter);
app.use("/customers", authenticationMiddleware.verifyToken);
app.use("/customers", customerRouter);
app.use("/", () => {
  console.log("Everything is fine...");
});
app.listen(port, (error) => {
  if (error) {
    console.log(error);
    throw error;
  } else {
    console.log("listening on port " + port);
  }
});
